CREATE TABLE Users
(
	Id int IDENTITY(1,1) PRIMARY KEY,
	UserName varchar(255) UNIQUE,
	GamePassword varchar(32)
);

CREATE TABLE UserStatistics
(
	Id_User int,
	Scope int,
	IsWin int
);
