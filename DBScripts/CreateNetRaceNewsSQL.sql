CREATE TABLE News
(
	Id int IDENTITY(1,1) PRIMARY KEY,
	Name varchar(128),
	Content varchar(512),
	CreateTime datetime
);