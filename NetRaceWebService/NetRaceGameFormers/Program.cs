﻿using System;
using System.Threading;
using NetRaceGameDataAccess;

namespace NetRaceGameFormers
{
    class Program
    {
        static void Main()
        {
            var formerThread = new Thread(Former);
            formerThread.Start();

            Console.WriteLine("Press something for exit.");
            var key = Console.ReadLine();
            formerThread.Abort();
        }

        static void Former()    
        {
            while (true)
            {
                var queuePlayers = Repository.GetPlayersInQueue();

                if (queuePlayers.Count > 1)
                {
                    for (var i = 0; i < queuePlayers.Count; i++)
                    {
                        if(i == queuePlayers.Count - 1) break;

                        var serverPlayer = queuePlayers[i];
                        var clientPlayer = queuePlayers[i + 1];

                        Repository.RemovePlayerFromQueue(serverPlayer.Name);
                        Repository.RemovePlayerFromQueue(clientPlayer.Name);

                        Repository.AddGameData(new GameData
                        {
                            ClientIp = clientPlayer.Ip,
                            ClientPlayerName = clientPlayer.Name,
                            ServerIp = serverPlayer.Ip,
                            ServerPlayerName = serverPlayer.Name,
                            IsServerStarted = false
                        });
                    }
                }

                Thread.Sleep(1000);
            }
        }

        private static MongoRepository Repository = new MongoRepository();
    }
}
