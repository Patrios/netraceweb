﻿using System.Runtime.Serialization;
using System.ServiceModel;

namespace NetRaceWebService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface IUserService
    {

        [OperationContract]
        bool IsRegistrated(string userName, string userPassword);

        [OperationContract]
        PlayerInformation GetOpponent(string userName, string userIp);

        [OperationContract]
        void SetServerStarted(string userName);

        [OperationContract]
        bool SaveScope(string userName, int scope, bool isWin);
    }

    [DataContract]
    public class PlayerInformation
    {
        private string _name;
        private string _ip;
        private bool _isServer;

        [DataMember]
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        [DataMember]
        public string IP
        {
            get { return _ip; }
            set { _ip = value; }
        }

        [DataMember]
        public bool IsServer
        {
            get { return _isServer; }
            set { _isServer = value; }
        }
    }
}
