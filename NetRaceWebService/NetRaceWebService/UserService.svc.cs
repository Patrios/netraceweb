﻿using System.Threading;
using NetRaceGameDataAccess;

namespace NetRaceWebService
{
    public class UserService : IUserService
    {
        public bool IsRegistrated(string userName, string userPassword)
        {
            return _repository.IsUserExists(userName, userPassword);
        }

        public PlayerInformation GetOpponent(string userName, string userIp)
        {
            _mongoRepository.AddUserToQueue(userName, userIp);

            GameData game = null;
            while (game == null)
            {
                Thread.Sleep(1000);
                game = _mongoRepository.GetGameWithPlayer(userName);
            }

            if (game.ServerPlayerName == userName)
            {
                return new PlayerInformation { IsServer = false };
            }

            while (!game.IsServerStarted)
            {
                Thread.Sleep(1000);
                game = _mongoRepository.GetGameWithPlayer(userName);
            }

            _mongoRepository.RemoveGameData(game);
            return new PlayerInformation { IP = game.ServerIp, IsServer = true };
        }

        public void SetServerStarted(string userName)
        {
            _mongoRepository.SetServerStarted(userName);
        }
        
        public bool SaveScope(string userName, int scope, bool isWin)
        {
            return _repository.SaveUserScope(userName, scope, isWin);
        }

        private readonly SqlRepository _repository = new SqlRepository();
        private readonly MongoRepository _mongoRepository = new MongoRepository();
    }
}
