﻿namespace NetRaceGameDataAccess.Model
{
    public sealed class UserStatistics
    {
        public UserStatistics(bool isWin, int scope)
        {
            IsWin = isWin;
            Scope = scope;
        }

        public bool IsWin { get; private set; }

        public int Scope { get; private set; }
    }
}
