﻿using System;

namespace NetRaceGameDataAccess.Model
{
    public sealed class News
    {
        public News (int id, DateTime date, string name, string content)
        {
            Id = id;
            Date = date;
            Name = name;
            Content = content;
        }

        public int Id { get; private set;  }

        public DateTime Date { get; private set; }

        public string Name { get; private set; }

        public string Content { get; private set; }


    }
}
