﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using NetRaceGameDataAccess.Model;

namespace NetRaceGameDataAccess
{
    public sealed class SqlRepository
    {
        public bool IsUserExists(string userName, string userPassword)
        {
            using (var connection = SqlServerConnectionanager.GetConnection())
            {
                const string query = "select * from Users where UserName = @userName and GamePassword = @gamePassword";
                var command = new SqlCommand(query, connection);
                command.Parameters.AddWithValue("@userName", userName);
                command.Parameters.AddWithValue("@gamePassword", userPassword);

                return command.ExecuteScalar() != null;
            }
        }

        public bool SaveUserScope(string userName, int scope, bool isWin)
        {
            using (var connection = SqlServerConnectionanager.GetConnection())
            {
                var userId = GetUserIdByName(userName, connection);
                const string query = "insert into UserStatistics (Id_User, Scope, IsWin) values (@idUser, @scope, @isWin)";
                var command = new SqlCommand(query, connection);
                command.Parameters.AddWithValue("@idUser", userId);
                command.Parameters.AddWithValue("@scope", scope);
                command.Parameters.AddWithValue("@isWin", Convert.ToInt32(isWin));

                return command.ExecuteNonQuery() == 1;
            }
        }

        public bool RegistrateUser(string name, string password)
        {
            using (var connection = SqlServerConnectionanager.GetConnection())
            {
                const string query = "insert into Users  (UserName, GamePassword) values (@name, @password)";
                var command = new SqlCommand(query, connection);
                command.Parameters.AddWithValue("@name", name);
                command.Parameters.AddWithValue("@password", password);

                return command.ExecuteNonQuery() == 1;
            }
        }

        public IList<News> GetNews()
        {
            using (var connection = SqlServerConnectionanager.GetConnection())
            {
                const string query = "select * from News";
                var command = new SqlCommand(query, connection);

                using (var reader = command.ExecuteReader())
                {
                    return MapListOfNews(reader).ToList();
                }
            }
        }

        public IList<UserStatistics> GetUserStatisticses(int userId)
        {
            using (var connection = SqlServerConnectionanager.GetConnection())
            {
                const string query = "select * from UserStatistics where Id_User = @userId";
                var command = new SqlCommand(query, connection);
                command.Parameters.AddWithValue("@userId", userId);

                using (var reader = command.ExecuteReader())
                {
                    return MapListOfUserStatistics(reader).ToList();
                }
            }
        }

        public int GetUserIdByName(string userName)
        {
            using (var connection = SqlServerConnectionanager.GetConnection())
            {
                return GetUserIdByName(userName, connection);
            }
        }

        private int GetUserIdByName(string userName, SqlConnection connection)
        {
            const string query = "select * from Users where UserName = @userName";
            var command = new SqlCommand(query, connection);
            command.Parameters.AddWithValue("@userName", userName);

            using (var reader = command.ExecuteReader())
            {
                return reader.Read() ? reader.GetInt32(0) : -1;
            }
        }
        private IEnumerable<UserStatistics> MapListOfUserStatistics(SqlDataReader reader)
        {
            while (reader.Read())
            {
                yield return MapUserStatistics(reader);
            }
        }

        private static IEnumerable<News> MapListOfNews(SqlDataReader reader)
        {
            while (reader.Read())
            {
                yield return MapNews(reader);
            }
        }

        private UserStatistics MapUserStatistics(SqlDataReader reader)
        {
            return new UserStatistics(
                (int)reader["IsWin"] == 1,
                (int)reader["Scope"]);
        }


        private static News MapNews(SqlDataReader reader)
        {
            return new News(
                (int)reader["Id"], 
                (DateTime)reader["CreateTime"], 
                reader["Name"].ToString(), 
                reader["Content"].ToString());
        }
    }
}