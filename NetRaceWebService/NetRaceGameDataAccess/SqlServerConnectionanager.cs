﻿using System.Configuration;
using System.Data.SqlClient;

namespace NetRaceGameDataAccess
{
    internal static class SqlServerConnectionanager
    {
        public static SqlConnection GetConnection()
        {
            var connection = new SqlConnection(ConnectionString);
            connection.Open();
            return connection;
        }

        private static string ConnectionString
        {
            get { return ConfigurationManager.AppSettings["SqlServerConnectionString"]; }
        }
    }
}