﻿using System.Collections.Generic;
using System.Linq;
using MongoDB.Driver;
using MongoDB.Driver.Builders;

namespace NetRaceGameDataAccess
{
    public class MongoRepository
    {
        public IList<QueuePlayer> GetPlayersInQueue()
        {
            var cursor = GetQueueCollection().FindAll();
            return cursor.Select(x => x).ToList();
        }

        public void RemovePlayerFromQueue(string user)
        {
            GetQueueCollection().Remove(Query<QueuePlayer>.EQ(x => x.Name, user));
        }
            
        public void AddUserToQueue(string userName, string userIp)
        {
            GetQueueCollection().Insert(new QueuePlayer
            {
                Name = userName, Ip = userIp
            });
        }

        public void AddGameData(GameData game)
        {
            GetGameDataCollection().Insert(game);
        }

        public GameData GetGameWithPlayer(string userName)
        {
            var game = GetGameDataCollection().FindOne(Query<GameData>.EQ(e => e.ClientPlayerName, userName));
            if (game != null) return game;

            return GetGameDataCollection().FindOne(Query<GameData>.EQ(e => e.ServerPlayerName, userName));
        }

        public void RemoveGameData(GameData game)
        {
            GetGameDataCollection().Remove(Query<GameData>.EQ(e => e.Id, game.Id));
        }

        public void SetServerStarted(string userName)
        {
            var query = Query<GameData>.EQ(e => e.ServerPlayerName, userName);
            var update = Update<GameData>.Set(e => e.IsServerStarted, true);
            GetGameDataCollection().Update(query, update);
        }

        private static MongoCollection<GameData> GetGameDataCollection()
        {
            return GetDataBase().GetCollection<GameData>("Games");
        }

        private static MongoCollection<QueuePlayer> GetQueueCollection()
        {
            return GetDataBase().GetCollection<QueuePlayer>("GameQueue");
        }

        private static MongoDatabase GetDataBase()
        {
            const string connectionString = "mongodb://localhost";
            var client = new MongoClient(connectionString);
            var server = client.GetServer();
            return server.GetDatabase("NetRaceGameData");
        }
    }
}
