﻿using MongoDB.Bson;

namespace NetRaceGameDataAccess
{
    public class QueuePlayer
    {
        public ObjectId Id { get; set; }

        public string Name { get; set; }

        public string Ip { get; set; }
    }
}
