﻿using System;
using MongoDB.Bson;

namespace NetRaceGameDataAccess
{
    public class GameData
    {
        protected bool Equals(GameData other)
        {
            return 
                string.Equals(ServerPlayerName, other.ServerPlayerName) && 
                string.Equals(ClientPlayerName, other.ClientPlayerName);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = (ServerPlayerName != null ? ServerPlayerName.GetHashCode() : 0);
                hashCode = (hashCode*397) ^ (ClientPlayerName != null ? ClientPlayerName.GetHashCode() : 0);
                return hashCode;
            }
        }
        public ObjectId Id { get; set; }

        public string ServerPlayerName { get; set; }

        public string ServerIp { get; set; }

        public string ClientPlayerName { get; set; }

        public string ClientIp { get; set; }

        public bool IsServerStarted { get; set; }


        public override bool Equals(Object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((GameData) obj);
        }
    }
}
