﻿$(document).ready(function () {
    $.template('news', $('#tmpl_news').html());

    loadedPage = 0;

    appendData = GetNews;

    appendData();

    $('.registration-form').hide();

    $(window).scroll(function () {
        if ($(window).scrollTop() + $(window).height() == $(document).height()) {
            appendData();
        }
    });

    $('#registrate-button').click(function() {
        $("#inputName").val('');
        $("#inputPassword").val('');
        $('.error-message').text('');
        $('.registration-answer').text('');
        $('.registration-form').show();
    });

    $(".find-user-button").click(function () {
        $(".news").remove();

        searchedUser = $(".searched-user").val();
        loadedPage = 0;
        appendData = GetStatistics;
        $('#DataTitle').text(searchedUser + " statistics.");
        appendData();
    });

    $("#registrate").click(function() {
        var name = $("#inputName").val();
        var password = $("#inputPassword").val();

        $.getJSON("/Main/RegistrateUser?name=" + name + "&password=" + password,
            function (data) {
                if (data.errorMessage == '') {
                    $('.registration-form').hide();
                    $('.registration-answer').text("User registrated!");
                } else {
                    $('.error-message').text(data.errorMessage);
                }
            });
    });
});

function AppendNews(news) {
    $.tmpl('news', news).appendTo('.content-list');
}

function AppendStatistics(statistics) {
    $.tmpl('news', statistics).appendTo('.content-list');
}

function GetNews() {
    $.getJSON("/Main/GetSomeNews?page=" + loadedPage, function (data) {
        AppendNews(data.news);
    });

    loadedPage++;
}

function GetStatistics() {
    $.getJSON("/Main/GetSomeStatistics?name=" + searchedUser + "&page=" + loadedPage,
        function (data) {
            if (data.NoSuchUser == true) {
                $('#DataTitle').text("No such user!");
            } else {
                AppendStatistics(data.statistics);
            }
        });

    loadedPage++;
}

var appendData;
var loadedPage;
var searchedUser = "";