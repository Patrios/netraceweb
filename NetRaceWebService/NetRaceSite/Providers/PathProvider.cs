﻿using System.IO;
using System.Web;

namespace NetRaceSite.Providers
{
    public static class PathProvider
    {
        public static string GetNewsPathToPhoto(int newsId)
        {
            const string templatePath = "/News/{0}/News.png";

            var newsPhoto = string.Format(templatePath, newsId);

            if (!File.Exists(HttpContext.Current.Server.MapPath(newsPhoto)))
            {
                return string.Format(templatePath, "Default");
            }

            return newsPhoto;
        }

        public static string GetStatisticsPhoto(bool isWin)
        {
            const string winName = "win.png";
            const string looseName = "loose.png";

            const string prefix = "/Images/";

            return prefix + (isWin ? winName : looseName);
        }
    }
}