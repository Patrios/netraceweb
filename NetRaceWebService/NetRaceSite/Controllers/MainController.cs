﻿using System;
using System.Linq;
using System.Web.Mvc;
using NetRaceGameDataAccess;
using NetRaceSite.Enums;
using NetRaceSite.Providers;

namespace NetRaceSite.Controllers
{
    public class MainController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult DownloadGame(Platforms platform)
        {
            var pathToRelise = String.Format("~/GameLastest/{0}.zip", platform);
            return File(pathToRelise, "application/zip", "NetRaceOnline.zip");
        }

        public JsonResult GetSomeNews(int page)
        {
            const int displayCount = 4;

            var news = _dataRepository
                .GetNews()
                .OrderByDescending(x => x.Date)
                .Skip(displayCount*page)
                .Take(displayCount);

            var jsonObjectList = news.Select(x => new
            {
                newsTitle = x.Name,
                newsContent = x.Content,
                pathToPhoto = PathProvider.GetNewsPathToPhoto(x.Id)
            }).ToArray();

            return Json(new { news = jsonObjectList }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetSomeStatistics(string name, int page)
        {
            const int displayCount = 4;

            var userId = _dataRepository.GetUserIdByName(name);

            if (userId == -1)
            {
                return Json(new { NoSuchUser = true }, JsonRequestBehavior.AllowGet);
            }

            var statisticses = _dataRepository
                .GetUserStatisticses(userId)
                .Skip(displayCount * page)
                .Take(displayCount);

            var jsonObjectList = statisticses.Select(x => new
            {
                newsTitle = x.IsWin ? "You Win!" : "You Loose!",
                newsContent = "You scope is: " + x.Scope,
                pathToPhoto = PathProvider.GetStatisticsPhoto(x.IsWin)
            }).ToArray();

            return Json(new { NoSuchUser = false, statistics = jsonObjectList }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult RegistrateUser(string name, string password)
        {
            if (_dataRepository.GetUserIdByName(name) != -1)
            {
                return Json(new { errorMessage = "There is user with such name!" }, JsonRequestBehavior.AllowGet);
            }

            if (string.IsNullOrEmpty(password) || string.IsNullOrEmpty(name))
            {
                return Json(new { errorMessage = "Password and name can not be null or empty!" }, JsonRequestBehavior.AllowGet);
            }

            if (!_dataRepository.RegistrateUser(name, password))
            {
                return Json(new { errorMessage = "Some problem with registration!" }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { errorMessage = "" }, JsonRequestBehavior.AllowGet);
        }

        private readonly SqlRepository _dataRepository = new SqlRepository();
    }
}
